/**
 * @module confluence/licensedetails
 */
define('confluence/licensedetails', [
    'jquery',
    'ajs',
    'aui/form-validation'
], function(
    $,
    AJS,
    validator
) {
    'use strict';

    return function LicenseDetails() {
        var flagBody = AJS.I18n.getText('licence.update.flag.body', AJS.I18n.getText('license.update.flag.learn.more.link'), AJS.I18n.getText('license.update.flag.clustering.link'));

        var contextPath = AJS.contextPath();
        validator.register(['validlicense'], function(field) {
            AJS.safe.post(
                contextPath + '/rest/license/1.0/license/validate',
                { licenseKey: field.$el.val() },
                function() {
                    field.validate();
                }
            ).fail(function(jqXHR) {
                field.invalidate(jqXHR.responseText);
            });
        });

        if (window.location.hash === '#updateSuccessful') {
            $.get(contextPath + '/rest/license/1.0/license/details',
                function(licenseDetails) {
                    if (licenseDetails.dataCenter) {
                        AJS.flag({
                            type: 'success',
                            title: AJS.I18n.getText('licence.update.flag.title'),
                            body: flagBody,
                            close: 'manual',
                        });
                    }
                }
            );

            window.location.hash = '';
        }
    };
});

require('confluence/module-exporter').safeRequire('confluence/licensedetails', function(LicenseDetails) {
    'use strict';

    var AJS = require('ajs');

    AJS.toInit(LicenseDetails);
});
