/**
 * @module confluence/help-panel
 */
define('confluence/help-panel', [
    'jquery',
    'window'
], function(
    $,
    window
) {
    'use strict';

    var HelpPanel = {};

    HelpPanel.initialize = function() {
        $('.help-panel-content a').click(function() {
            var linkUrl = $(this).attr('href');
            var onClickEvent = $(this).attr('onClick');
            if (!onClickEvent && linkUrl && linkUrl.indexOf('#') !== 0 && linkUrl.indexOf(window.location) === -1) {
                window.open(linkUrl, '_blank').focus();
                return false;
            }
        });
    };
    return HelpPanel;
});

require('confluence/module-exporter').safeRequire('confluence/help-panel', function(HelpPanel) {
    'use strict';

    var AJS = require('ajs');

    AJS.toInit(HelpPanel.initialize);
});
