/**
 * @module raphael
 */
define("raphael", function () {
    "use strict";
    return Raphael;
});