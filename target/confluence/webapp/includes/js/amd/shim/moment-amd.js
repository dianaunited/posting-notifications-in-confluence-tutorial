/**
 * @module moment
 */
define("moment", function () {
    "use strict";
    return moment;
});