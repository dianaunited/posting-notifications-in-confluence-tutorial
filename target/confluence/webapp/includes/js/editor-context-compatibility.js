/**
 * Compatibility for loading editor-v3 or editor-v4 automatically when editor is requested.
 * @module confluence/editor-context-compatibility
 */
define('confluence/editor-context-compatibility', [
    'ajs',
    'wrm',
    'confluence/dark-features'
], function(AJS, WRM, DarkFeatures) {
    'use strict';

    var EDITOR_CONTEXT = 'editor';
    var EDITOR_V3_CONTEXT = 'editor-v3';
    var EDITOR_V4_CONTEXT = 'editor-v4';
    var EDITOR_V4_DARK_FEATURE = 'frontend.editor.v4';
    var EDITOR_V4_COMPATIBILITY_DARK_FEATURE = 'frontend.editor.v4.compatibility';
    var originalWrmRequire = WRM.require;
    var alreadyBootstrapped = false;

    /**
     * Main
     */
    function bootstrap() {
        if (alreadyBootstrapped) {
            return;
        }

        if (!DarkFeatures.isEnabled(EDITOR_V4_COMPATIBILITY_DARK_FEATURE)) {
            return;
        }

        AJS.log('Editor V4 Compatibility mode enabled.');

        WRM.require = wrappedRequire;
        AJS.log('Replaced WRM.require by a wrapper function to provide compatibility for'
            + ' the editor upgrade.');

        alreadyBootstrapped = true;
    }

    // See require-handler.js in WRM
    function wrappedRequire(resources, callback) {
        var resourcesArray = Array.isArray(resources) ? resources : [resources];
        var editorContextIndex = resourcesArray.indexOf('wrc!' + EDITOR_CONTEXT);
        if (editorContextIndex !== -1) {
            var supportingContext = DarkFeatures.isEnabled(EDITOR_V4_DARK_FEATURE)
                ? EDITOR_V4_CONTEXT
                : EDITOR_V3_CONTEXT;
            var supportingContextForWRM = 'wrc!' + supportingContext;
            if (resourcesArray.indexOf(supportingContextForWRM) === -1) {
                resourcesArray.splice(editorContextIndex, 0, supportingContextForWRM);
                AJS.log('Since \'editor\' context is requested, also going to request \'' + supportingContext + '\'.');
            }
        }

        return originalWrmRequire(resourcesArray, callback);
    }

    return {
        bootstrap: bootstrap
    };
});

require('confluence/module-exporter').safeRequire('confluence/editor-context-compatibility', function(Compatibility) {
    'use strict';

    Compatibility.bootstrap();
});
