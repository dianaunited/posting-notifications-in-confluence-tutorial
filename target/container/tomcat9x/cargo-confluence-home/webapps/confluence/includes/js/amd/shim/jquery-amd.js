/**
 * @module jquery
 */
define('jquery', function() {
    "use strict";
    return jQuery;
});
