/**
 * AMD Wrapper for the tinymce window global.
 * @module tinymce
 */
define("tinymce", ['window'], function (window) {
    "use strict";
    // This is a 'confluence/dark-features' implementation.
    // The goal is not to introduce dependency from core on a module from a bundled plugin.
    // tinymce module must also no depend on the editor directly as we need to have editor loaded conditionally based on DF.
    // This code will go away after 7.0 is released - see CONFSRVDEV-3990.
    var darkFeaturesTag = window.document.head.querySelector("meta[name='ajs-enabled-dark-features']");
    if (darkFeaturesTag && darkFeaturesTag.hasAttribute('content')) {
        var darkFeatures = darkFeaturesTag.getAttribute('content').split(',');
        if (darkFeatures.indexOf('frontend.editor.v4') !== -1 && darkFeatures.indexOf('frontend.editor.v4.disable') === -1) {
            require('confluence-editor-v4/build/editor');
        }
    }
    return tinymce;
});
