define("confluence/page-move-dialog","ajs confluence/templates confluence/legacy confluence/meta window jquery confluence/breadcrumbs wrm wrm/context-path confluence/message-controller confluence/form-state-control confluence/api/ajax confluence/api/event".split(" "),function(d,f,u,e,F,a,z,G,r,A,k,B,l){return function(){function C(){a("#move-page-dialog").find(".button-spinner").each(function(b,d){a(d).spin()})}function v(){a("#move-page-dialog").find(".button-spinner").each(function(b,d){a(d).spinStop()})}
function H(b,e,c,f,j,i,o){function n(a){o(a);k.enableElement([y,h,p,s,g]);v()}a("#move-errors").remove();C();var b=a("#move-page-dialog"),y=b.find(".move-button")[0],h=b.find("button.reorder-button")[0],p=b.find(".button-panel-cancel-link")[0],s=b.find(".dialog-back-link")[0],g=b.find("#reorderCheck")[0];k.disableElement(y);a("#createpageform").length>0?w||k.disableElement([h,p,s,g]):k.disableElement([h,p,s,g]);B.ajax({url:r()+"/pages/movepage.action",type:"POST",dataType:"json",timeout:18E4,data:new D(e,
f,j,i),error:function(a){n(A.parseError(a,d.I18n.getText("move.page.dialog.move.failed")))},success:function(a){var b=[].concat(a.validationErrors||[]).concat(a.actionErrors||[]).concat(a.errorMessage||[]);b.length>0?n(b):F.location.href=r()+a.page.url+(a.page.url.indexOf("?")>=0?"&":"?")+"moved=true"}})}var w=d.DarkFeatures.isEnabled("editor.ajax.save")&&!d.DarkFeatures.isEnabled("editor.ajax.save.disable"),t=function(b){function x(b){var d=a("#move-errors");d.length>0&&d.remove();var d=a(f.MovePage.errorMessage()),
c=g.find(".browse-controls:visible");if(c.length)c.append(d);else{c=g.find(".dialog-panel-body:visible");c.prepend(d)}if(!b||b.length===0)a(s).prop("disabled",false);else{b=a.isArray(b)&&b.length>1?f.MovePage.errorList({errors:b}):b;b=aui.message.error({content:b});d.html(b);d.removeClass("hidden")}}var c=e.get("page-title"),b=a.extend({spaceKey:e.get("space-key"),spaceName:e.get("space-name"),pageTitle:c,parentPageTitle:e.get("parent-page-title"),title:d.I18n.getText("move.page.dialog.title.view",
c),buttonName:d.I18n.getText("move.name"),openedPanel:d.I18n.getText("move.page.dialog.panel.location"),moveHandler:function(a){d.debug("No move handler defined. Closing dialog.");a.remove()},cancelHandler:function(a){a.remove();return false}},b),m=b.spaceKey,j=b.spaceName,i=b.parentPageTitle,o="",n="",y=function(a,b){o=a;n=b},c=d.ConfluenceDialog({width:840,height:590,id:"move-page-dialog"}),h={spaceKey:m,spaceName:j,parentPageTitle:i,canMoveBetweenSpaces:e.get("page-id")==="0"||e.getBoolean("can-remove-page"),
canMoveHierarchyBetweenSpaces:e.get("page-id")==="0"||e.getBoolean("can-remove-page-hierarchy")};c.addHeader(b.title);c.addPanel(d.I18n.getText("move.page.dialog.panel.location"),f.MovePage.movePageAdvancedPanel(h),"location-panel","location-panel-id");c.addPanel(d.I18n.getText("move.page.dialog.search.title"),f.MovePage.movePageSearchPanel(h),"search-panel","search-panel-id");c.addPanel(d.I18n.getText("move.page.dialog.history.title"),f.MovePage.historyPanel({pageTitle:e.get("page-title")}),"history-panel",
"history-panel-id");c.addPanel(d.I18n.getText("move.page.dialog.browse.title"),f.MovePage.browsePanel({pageTitle:e.get("page-title")}),"browse-panel","browse-panel-id");c.get('#"'+d.I18n.getText("move.page.dialog.panel.location")+'"')[0].onselect=function(){a("#new-space-key").val(m);a("#new-space").val(j);a("#new-parent-page").val(i).select()};c.get('#"'+d.I18n.getText("move.page.dialog.search.title")+'"')[0].onselect=function(){var b=a("#move-page-dialog");b.find(".search-panel .search-results .selected").removeClass("selected");
b.find("input.search-query").focus()};c.get('#"'+d.I18n.getText("move.page.dialog.history.title")+'"')[0].onselect=function(){a(".history-panel",g).movePageHistory(q)};c.get('#"'+d.I18n.getText("move.page.dialog.browse.title")+'"')[0].onselect=function(){d.debug("browse: "+[m,j,i].join());a(".browse-panel",g).movePageBrowse(q,m,j,i,t,b.pageTitle)};var p=function(c){var h=a("#new-space:visible").val(),e=a("#new-space-key").val(),g=a("#new-parent-page:visible").val();if(h&&(h!==j||e!==m||g!==i))u.Dialogs.Breadcrumbs.defaultGetBreadcrumbs({spaceKey:e,
pageTitle:g},function(){u.PageLocation.set({spaceKey:e,spaceName:h,parentPageTitle:g});b.moveHandler(c,e,h,g,o,n,x)},function(b){a("#new-parent-breadcrumbs").html(f.MovePage.breadcrumbError());b.status===404&&q.error(d.I18n.getText("move.page.dialog.location.not.found"))});else{u.PageLocation.set({spaceKey:m,spaceName:j,parentPageTitle:i});b.moveHandler(c,m,j,i,o,n,x)}};c.addButton(b.buttonName,function(c){if(a("#createpageform").length>0&&d.DarkFeatures.isEnabled("editor.ajax.save")&&!d.DarkFeatures.isEnabled("editor.ajax.save.disable"))p(c);
else if(a("#reorderCheck")[0].checked){c.nextPage();c=a("#move-page-dialog");a(".ordering-panel",c).movePageOrdering(m,i,b.pageTitle,y)}else p(c)},"move-button aui-button aui-button-primary");a(".button-panel-button.move-button").attr("id","move-button");c.addCancel(d.I18n.getText("cancel.name"),b.cancelHandler);c.popup.element.find(".dialog-title").prepend(f.MovePage.helpLink());a("#createpageform").length>0?w||c.addPage().addHeader(b.title).addPanel(d.I18n.getText("move.page.dialog.ordering.title"),
f.MovePage.orderingPagePanel(),"ordering-panel","ordering-panel-id").addLink(d.I18n.getText("move.page.dialog.back.button"),function(a){a.prevPage()},"dialog-back-link").addButton(d.I18n.getText("move.page.dialog.order.button"),p,"reorder-button").addCancel(d.I18n.getText("cancel.name"),b.cancelHandler):c.addPage().addHeader(b.title).addPanel(d.I18n.getText("move.page.dialog.ordering.title"),f.MovePage.orderingPagePanel(),"ordering-panel","ordering-panel-id").addLink(d.I18n.getText("move.page.dialog.back.button"),
function(a){a.prevPage()},"dialog-back-link").addButton(d.I18n.getText("move.page.dialog.order.button"),p,"reorder-button").addCancel(d.I18n.getText("cancel.name"),b.cancelHandler);var s=c.get("button#"+b.buttonName)[0].item;if(a("#createpageform").length>0&&w)a("button.move-button").before(f.MovePage.spinnerButton());else{a("button.move-button").before(f.MovePage.reorderCheckbox());a("button.reorder-button").before(f.MovePage.spinnerButton());a("#reorderRequirement").before(f.MovePage.spinnerButton())}c.gotoPage(0);
var g=a("#move-page-dialog"),l=g.find(".dialog-page-menu"),h=g.find(".dialog-page-body"),k=a(l[0]),r=a(h[0]);r.height(k.height());r.width("75%");r.find(".dialog-panel-body").height(k.height()-71);l=a(l[1]);h=a(h[1]);l.width("0");h.width("100%");c.show();a(".location-panel .location-info",g).appendTo(a(".dialog-page-body:first",g));var v=new u.Dialogs.Breadcrumbs.Controller(a("#new-parent-breadcrumbs")),q={moveButton:s,clearErrors:function(){x([])},error:x,select:function(b,c,h,e){d.debug("select: "+
[b,c,h].join());a("#createpageform").length>0&&w&&typeof e!=="undefined"&&a("#parentPageId").val(e);m=b;j=c;i=h||"";a(s).prop("disabled",true);v.update({spaceKey:m,title:i},q)}};c.overrideLastTab();c.get('#"'+b.openedPanel+'"').select();var t=e.get("parent-page-title")||e.get("from-page-title");(new u.Dialogs.Breadcrumbs.Controller(a("#current-parent-breadcrumbs"))).update({spaceKey:e.get("space-key"),title:i||t},q);a(".location-panel",g).movePageLocation(q);a(".search-panel",g).movePageSearch(q);
a(".history-panel",g).movePageHistory(q);a("#new-parent-page").select();b.hint&&c.addHelpText(b.hint.template||b.hint.text,b.hint.arguments);G.require("wr!com.atlassian.confluence.plugins.confluence-page-restrictions-dialog:dialog-resources");return g},D=function(a,d,c,f){a={pageId:e.get("content-id")||e.get("page-id"),spaceKey:a};if(c){a.position=f;a.targetId=c}else if(d!==""){a.targetTitle=d;a.position="append"}else a.position="topLevel";return a};l.bind("deferred.page-move.tools-menu",function(b){b.preventDefault();
a("#move-page-dialog").length>0&&a("#move-page-dialog, body > .shadow, body > .aui-blanket").remove();new t({moveHandler:H});return false});var E;l.bind("deferred.page-move.editor",function(b){b.preventDefault();a("#move-page-dialog").length>0&&a("#move-page-dialog, body > .shadow, body > .aui-blanket").remove();new t({spaceName:E,spaceKey:a("#newSpaceKey").val(),pageTitle:a("#content-title").val(),parentPageTitle:a("#parentPageString").val(),buttonName:d.I18n.getText("move.name"),title:d.I18n.getText("move.page.dialog.title.edit"),
moveHandler:function(b,c,f,j,i,o,n){function k(){E=f;a("#newSpaceKey").val(c);a("#parentPageString").val(j);j!==""?a("#position").val("append"):a("#position").val("topLevel");if(a("#createpageform").length>0&&w){var b=a("#parentPageId");typeof b!=="undefined"&&typeof b.val()!=="undefined"&&e.set("parent-page-id",b.val());typeof c!=="undefined"&&e.set("space-key",c)}if(i){a("#targetId").val(i);a("#position").val(o)}}if(e.get("shared-drafts")){C();B.ajax({url:r()+"/pages/movepage.action",type:"POST",
dataType:"json",data:new D(c,j,i,o),error:function(a){v();n(A.parseError(a,d.I18n.getText("move.page.dialog.move.failed")))},success:function(a){v();a=[].concat(a.validationErrors||[]).concat(a.actionErrors||[]).concat(a.errorMessage||[]);if(a.length>0)n(a);else{k();b.remove();z.update(c,j);l.trigger("editor-page-moved")}}})}else{k();b.remove();z.update(c,j);l.trigger("editor-page-moved")}}});return false});l.trigger("page.move.dialog.ready");return t}});
require("confluence/module-exporter").safeRequire("confluence/page-move-dialog",function(d){var f=require("confluence/legacy");require("ajs").toInit(function(){f.MovePageDialog=d()})});