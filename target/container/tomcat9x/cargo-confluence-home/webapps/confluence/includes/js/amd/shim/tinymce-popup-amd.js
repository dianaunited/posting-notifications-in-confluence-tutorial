/**
 * AMD Wrapper for the tinyMCEPopup global
 * @module tinymce/popup
 */
define("tinymce/popup", function () {
    if (typeof tinyMCEPopup === "undefined") {
        return {};
    } else {
        return tinyMCEPopup;
    }
});