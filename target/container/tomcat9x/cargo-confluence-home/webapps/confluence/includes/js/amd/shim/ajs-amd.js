/**
 * AMD Shim around the AJS window global.
 *
 * @module ajs
 *
 * @see {@link http://developer.atlassian.com/aui/}
 */
define("ajs", function () {
    "use strict";
    return AJS;
});