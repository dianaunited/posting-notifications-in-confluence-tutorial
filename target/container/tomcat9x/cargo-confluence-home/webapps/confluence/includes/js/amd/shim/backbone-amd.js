/**
 * @module backbone
 */
define('backbone', ['underscore', 'jquery'], function () {
    "use strict";
    return Backbone;
});