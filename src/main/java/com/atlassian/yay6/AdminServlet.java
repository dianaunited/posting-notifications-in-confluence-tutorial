package com.atlassian.yay6;

import java.io.IOException;
import java.net.URI;

import javax.inject.Inject;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.sal.api.auth.LoginUriProvider;


import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class AdminServlet extends HttpServlet
{

    private static final Logger log = LoggerFactory.getLogger(NotificationResource.class);
    @ComponentImport
    private final PermissionManager permissionManager;
    @ComponentImport
    private final TemplateRenderer renderer;
    @ComponentImport
    private final LoginUriProvider loginUriProvider;

    @Inject
    public AdminServlet(PermissionManager permissionManager, TemplateRenderer renderer, LoginUriProvider loginUriProvider)
    {
        this.permissionManager = permissionManager ;
        this.renderer = renderer;
        this.loginUriProvider = loginUriProvider;
        log.info("Info Message!");
        log.error("Error Message!");
        log.debug("Debug Message!");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException
    {
        if (!isAdmin())
        {
            redirectToLogin(req, resp);
            return;
        }

        resp.setContentType("text/html;charset=utf-8");
        renderer.render("templates/admin.vm", resp.getWriter());
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    private boolean isAdmin()
    {
        return permissionManager.isConfluenceAdministrator(AuthenticatedUserThreadLocal.get());
    }

}

