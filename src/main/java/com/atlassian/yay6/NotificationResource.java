package com.atlassian.yay6;

import java.util.concurrent.ExecutionException;
import java.util.stream.StreamSupport;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.Settings;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.mywork.model.Notification;
import com.atlassian.mywork.model.NotificationBuilder;
import com.atlassian.mywork.service.LocalNotificationService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.user.impl.DefaultGroup;
import com.atlassian.user.search.page.Pager;

import com.google.common.collect.Iterables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * A resource of creating and listing notifications
 */
@Path ("/")
@Consumes ((MediaType.APPLICATION_JSON)) // prevents XSRF !
@Component
public class NotificationResource
{

    private static final Logger log = LoggerFactory.getLogger(NotificationResource.class);
    public static final int MAX_RESULT = 3;
    public static final String PLUGIN_KEY = "com.atlassian.yay6";
    @ComponentImport
    private final LocalNotificationService notificationService;
    @ComponentImport
    private final UserAccessor userAccessor;

    @ComponentImport
    private final PermissionManager permissionManager;

    @Autowired
    public NotificationResource(final LocalNotificationService notificationService, final UserAccessor userAccessor, final PermissionManager permissionManager)
    {
        this.notificationService = notificationService;
        this.userAccessor = userAccessor;
        this.permissionManager = permissionManager;
        log.info("Info Message!");
        log.error("Error Message!");
        log.debug("Debug Message!");
    }

    @POST
    public Response createNotification(@FormParam ("title") String title, @FormParam ("message") String message)
            throws Exception
    {
        if (isAdmin())
        {
            sendNotificationToAllUsers(title, message);
            return Response.ok().build();
        }
        else
        {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }

    @GET
    public Response findAllNotifications() {
        if (isAdmin()) {
            // find all the notifications received by the logged in user
            final Iterable<Notification> notifications = notificationService.findAll(AuthenticatedUserThreadLocal.get().getName());

            // we are only interested in the notification send by our plugin
            StreamSupport.stream(notifications.spliterator(), false);
            // Let's only display the last MAX_RESULT notifications
            return Response.ok(Iterables.limit(notifications, MAX_RESULT)).build();
        }
        else {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }

    private boolean isAdmin()
    {
        return permissionManager.isConfluenceAdministrator(AuthenticatedUserThreadLocal.get());
    }

    /**
     * Iterate on all users of the "confluence-user" group and send a notification to each of them
     * @param title the title of the notification to send
     * @param message the body of the notification to send
     * @throws ExecutionException exception
     * @throws InterruptedException exception
     */

    private void sendNotificationToAllUsers(final String title, final String message)
            throws ExecutionException, InterruptedException
    {
         //create instance of class because its non-static method being called from a static one
        Pager<String> memberNames = userAccessor.getMemberNames(new DefaultGroup(new Settings().getDefaultUsersGroup()));
        for (String memberName : memberNames)
        {
            sendNotification(memberName, title, message);
        }
    }

    /**
     * Create a single notification and send it to user
     * @param user the user who will receive the notification
     * @param title the title of the notification
     * @param message the body of the notification
     * @throws InterruptedException exception
     * @throws ExecutionException exception
     */
    private void sendNotification(final String user, final String title, final String message) throws InterruptedException, ExecutionException{
        notificationService.createOrUpdate(user, new NotificationBuilder()
                .application(PLUGIN_KEY) // a unique key that identifies your plugin
                .title("Message from your beloved administrator")
                .itemTitle(title)
                .description(message)
                .groupingId("com.atlassian.yay6") // a key to aggregate notifications
                .createNotification()).get();
    }

}
